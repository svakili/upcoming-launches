'use strict';

var express = require('express');
var router = express.Router();
const requestLaunches = require('../utils/requestLaunches');

/* GET home page. */
router.get('/', (req, res) => { // Initiate the homepage with 50 records
    requestLaunches(res, '50');
});

router.post('/', (req, res) => {
    requestLaunches(res, req.body.numberOfLaunches);
});

module.exports = router;
