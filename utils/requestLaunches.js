'use strict';
const request = require('request');

function requestLaunches(res, count) {
    request(`https://launchlibrary.net/1.2/launch/next/${count}`, (err, response, body) => {
        if (err) {
            console.log(err);
            return res.end(err.message);
        }
        
        if (response.statusCode == 200) {
            const launches = JSON.parse(body).launches;

            launches.forEach((launch) => {
                try {
                    launch.agency = launch.location.pads[0].agencies[0].name;
                    launch.agencyWiki = launch.location.pads[0].agencies[0].wikiURL;
                } catch (err) {
                    launch.agency = '';
                    launch.agencyWiki = '';
                }
            });
            res.render('index', {
                launches, count
            });
        } else {
            console.log('statusCode:', response && response.statusCode);
            return res.end();
        }
    });
}

module.exports = requestLaunches;
